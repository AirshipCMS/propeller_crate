/* The BSD 2-Clause Licence

Copyright (c) 2011, Propeller Contributors
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

  - Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
    
  - Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
package propeller_crate;

class Propeller
{
  // set this to find the partial template files
  public static var PARTIAL_SEARCH_PATHS = ["."];
  // set this to override the default
  public static var PARTIAL_EXTENSION = ".html";
  // set this to override the default
  public static var PARTIAL_FILE_PREFIX = "_";

  private static var spaced_exprs = ~/\{\{([a-zA-Z0-9&%?#<> \/'":;,._|\-=+\(\)\[\]]*)\}\}/g; // ~/{{(.*)}}/g breaks when multiple propeller pairs on same

  public static inline function render_html(_input:String, context:Dynamic):String
  {
    // allow spacing between propellers and expression
    // {{ expr }}
    _input = spaced_exprs.map(_input,
      function(r)
      {
        return '::${StringTools.trim(r.matched(1))}::';
      });

    // resolve any partials, without macros
    var partial_ereg = new EReg("([ ]*)::partial[ ]*['\"]([a-zA-Z0-9._/-]+)['\"][ ]*[,]*[ ]*[\\[]?([a-zA-Z0-9&?#<> \\/'\":;,._|\\-=+\\(\\)]*)[\\]]?::","gi");
    _input = partial_ereg.map(_input,
      function(r)
      {
        return render_partial(r.matched(2), r.matched(1), render_partial_hook, context, r.matched(3)); 
      }
    );
    
    // resolve any helper methods
    var helper_ereg = new EReg("::([A-Z0-9._-]+)\\[([A-Z0-9?#%&<>; '\":,.|_/=-]*)\\]::","gi");
    _input = helper_ereg.map(_input,
      function(r)
      {
        var matched_args = "";
        if(r.matched(2) != null){
          matched_args = r.matched(2);
        }

        // Template doesn't like extra commas
        var args = StringTools.trim(StringTools.replace(matched_args,",","%%"));
        
        return '$$$$run_helper(${r.matched(1)},$args,::__current__::)';
      }
    );

    // allow iterating over values returned by helper methods
    // foreach getBlogs[3]
    // actually returns a proper ::foreach iterable_name::
    var foreach_helper_ereg = new EReg("::foreach[ ]*([A-Z0-9._-]+)\\[([A-Z0-9?#&<>; '\":,.|_/=-]*)\\]::","gi");
    _input = foreach_helper_ereg.map(_input,
      function(r)
      {
        var matched_args = "";
        if(r.matched(2) != null){
          matched_args = r.matched(2);
        }
        
        // Template doesn't like extra commas
        var args = StringTools.trim(StringTools.replace(matched_args,",","%%"));
        return run_foreach_helper(r.matched(1),args,context);
      }
    );
    
    var template = new haxe.Template(_input);
    return template.execute(context, { run_helper : run_helper });
  }

  dynamic public static function render_partial_hook(input:String, filename:String):String
  {
    return input;
  }

  private static function render_partial( partial_name:String, prepend_indents:String, render_partial_hook:String->String->String, context:Dynamic, ?locals_str:String ):String
  {
    var partial_template = '[[PARTIAL TEMPLATE NOT FOUND: "${partial_name}"]]';
    var file_path = "";
    for(path in PARTIAL_SEARCH_PATHS){
      file_path = path + "/" +
          addPrefix(partial_name) +
          PARTIAL_EXTENSION;
      partial_template += '\n<br />[[LOOKED FOR "${partial_name}" in ${file_path}]]';
      if(sys.FileSystem.exists(file_path)
      ){
        partial_template = spaced_exprs.map(
          sys.io.File.getContent(file_path),
          function(r)
          {
            return '::${StringTools.trim(r.matched(1))}::';
          });
        break;
      }
    }

    // prepend leading spaces to each line
    // important for haml
    partial_template = partial_template.split("\n").map(
      function(a)
      {
        return '${prepend_indents}$a';
      }
    ).join("\n");

    // check for any locals
    var locals:Dynamic = {};
    if(locals_str != null && locals_str != ""){
      // parse locals
      locals = get_partial_locals(locals_str);
    }
    if(locals != {}){
      // if there are locals, map them to context
      for(local_name in Reflect.fields(locals)){
        var mapped_local_name = '${local_name}_${Std.string(Date.now().getTime())}_${Std.random(0xFFF)}';
        
        // save to context
        Reflect.setField(context,mapped_local_name,Reflect.field(locals,local_name));
        
        // replace propeller input vars to new mapped local name
        // check to make sure it's only in propellers !
        /// nuuuu partial_template = StringTools.replace(partial_template, local_name, mapped_local_name);
        var local_name_in_propellers = new EReg('(::.*)${local_name}(.*::)','gi'); // ~/{{(.*)}}/g breaks when multiple propeller pairs on same

        if(local_name_in_propellers.match(partial_template)){
          partial_template = local_name_in_propellers.replace(partial_template,'$1${mapped_local_name}$2');
        }
        
      }
      
    }

    // resolve any partials, without macros
    var partial_ereg = new EReg("([ ]*)::partial[ ]*['\"]([a-zA-Z0-9._/-]+)['\"][ ]*[,]*[ ]*[\\[]?([a-zA-Z0-9&?#<> \\/'\":;,._|\\-=+\\(\\)]*)[\\]]?::","gi");
    partial_template = partial_ereg.map(partial_template,
      function(r)
      {
        return render_partial(r.matched(2), r.matched(1), render_partial_hook, context, r.matched(3)); 
      }
    );
    
    // resolve any helper methods
    var helper_ereg = new EReg("::([A-Z0-9._-]+)\\[([A-Z0-9?#&<>; '\":,.|_/=-]*)\\]::","gi");
    partial_template = helper_ereg.map(partial_template,
      function(r)
      {
        var matched_args = "";
        if(r.matched(2) != null){
          matched_args = r.matched(2);
        }

        // Template doesn't like extra commas
        var args = StringTools.trim(StringTools.replace(matched_args,",","%%"));
        
        return '$$$$run_helper(${r.matched(1)},$args,::__current__::)';
      }
    );
    
    return render_partial_hook(partial_template, file_path);
  }
  private static inline function get_partial_locals(locals_str:String):Dynamic
  {
    var out = {};
    //"subpage_name"=>"Custom Partial Context", another => 2
    //var locals_ar = locals_str
    var locals_ereg = new EReg("(['\"]?[a-zA-Z0-9_\\-]*['\"]?[ ]*=>[ ]*['\"]?[a-zA-Z0-9&?#<> \\/'\":;,._\\-=+\\(\\)]*['\"]?)","gi");
    // var matched = locals_ereg.match(locals_str);
    // trace(matched);
    var locals_ar = [];
    locals_ereg.map(locals_str, function(a:EReg){
      locals_ar.push(a.matched(0));

      var pair = a.matched(0).split("=>");
      if(pair.length>1){
        var var_name = StringTools.trim(pair.shift());
        // scrub surrounding quotes
        // check if it's wrapped in double quotes
        var trim_quotes_ereg = ~/^"([ A-Z0-9\._\-]+)"$/i;
        // check if it's wrapped in single quotes
        var trim_quote_ereg = ~/^'([ "A-Z0-9\._\-]+)'$/i;
        
        if(trim_quotes_ereg.match(var_name)){
          var_name = trim_quotes_ereg.replace(var_name,"$1");
        }else if(trim_quote_ereg.match(var_name)){
          var_name = trim_quote_ereg.replace(var_name,"$1");
        }

        var content = StringTools.trim(pair.join("=>"));
        if(trim_quotes_ereg.match(content)){
          content = trim_quotes_ereg.replace(content,"$1");
        }else if(trim_quote_ereg.match(content)){
          content = trim_quote_ereg.replace(content,"$1");
        }        

        if(content == "true"){
          Reflect.setField(out,var_name,true);
        }else if(content == "false"){
          Reflect.setField(out,var_name,false);
        }else{
          Reflect.setField(out,var_name,content);
        }
        
      }

      return a.matched(0);
    });
    
    return out;
  }
  private static inline function addPrefix(file_path:String):String
  {
    if(file_path.indexOf("/") > 0){
      var parts = file_path.split("/");
      var last = parts.pop();
      file_path = 
        parts.join("/") + 
        "/" +
        PARTIAL_FILE_PREFIX +
        last;
    }else{
      file_path = PARTIAL_FILE_PREFIX + file_path;
    }
    return file_path;
  }

  private static function run_helper(resolve : String -> Dynamic, helper_name:String, helper_args:String, context:Dynamic):String
  {
    var args = [];
    if(helper_args != ""){
      // turn helper_args back into an array of args
      args = helper_args.split("%%").map(function(a){
        var r = resolve(a);
        if (r == null){
          // remove quotes
          // there's gotta be a better way to do this
          a = StringTools.trim(a);
          // check if it's wrapped in double quotes
          var trim_quotes_ereg = ~/^"([ 'A-Z0-9\.:|_\/\-=]+)"$/i;
          // check if it's wrapped in single quotes
          var trim_quote_ereg = ~/^'([ "A-Z0-9\.:|_\/\-=]+)'$/i;
          
          if(trim_quotes_ereg.match(a)){
            a = trim_quotes_ereg.replace(a,"$1");
          }else if(trim_quote_ereg.match(a)){
            a = trim_quote_ereg.replace(a,"$1");
          }else{
            // not wrapped in doubled quotes, force resolve it
            
            // LameResolve™
            var val:Dynamic = null;
            var parts = a.split('.');
                        
            val = Reflect.field(context,parts[0]);
            for (i in 1...parts.length) {
              val = Reflect.field(val,parts[i]);
            }
            
            // uncommenting this if statement
            // will turn null values into a string containing
            // the field name
            // if(val != null){
              a = val;
            // }
          }
          
          return a;
        }else{
          return r;  
        }
      });
    }
    
    // execute helper method
    // TODO, add error checking/reporting
    return Reflect.callMethod(null,resolve(helper_name),args);
  }

  // not a macro
  private static function run_foreach_helper(helper_name:String, helper_args:String, context:Dynamic):String
  {
    var sanitized_helper_args = helper_args.split("%%").map(function(a){
      // only allow letters and numbers for variable name
      return haxe.crypto.Md5.encode(a).substr(0,5);
    }).join("_");
    var loop_name = 'foreach_helper_${helper_name}_${ sanitized_helper_args }';

    // check if it's wrapped in double quotes
    var trim_quotes_ereg = ~/^"([ 'A-Z0-9\.:|_\/\-=]+)"$/i;
    // check if it's wrapped in single quotes
    var trim_quote_ereg = ~/^'([ "A-Z0-9\.:|_\/\-=]+)'$/i;
    
    if(!Reflect.hasField(context, loop_name)){

      var args = [];
      if(helper_args != ""){
        // turn helper_args back into an array of args
        args = helper_args.split("%%").map(function(a){
          var subtemplate = new haxe.Template('::$a::');
          var r = subtemplate.execute(context, { run_helper : run_helper });

          if (r == null || r == "null"){
            // remove quotes
            // there's gotta be a better way to do this
            a = StringTools.trim(a);
            
            if(trim_quotes_ereg.match(a)){
              a = trim_quotes_ereg.replace(a,"$1");
            }else if(trim_quote_ereg.match(a)){
              a = trim_quote_ereg.replace(a,"$1");
            }else{
              // not wrapped in doubled quotes, force resolve it
              
              // LameResolve™
              var val:Dynamic = null;
              var parts = a.split('.');
                          
              val = Reflect.field(context,parts[0]);
              for (i in 1...parts.length) {
                val = Reflect.field(val,parts[i]);
              }
              
              if(val != null){
                a = val;
              }
            }
            
            return a;
          }else{
            // not parsed by Template
            // remove ::dots::
            // got lazy
            if(trim_quotes_ereg.match(r.split("::").join(""))){
              r = trim_quotes_ereg.replace(a,"$1");
            }else if(trim_quote_ereg.match(a)){
              r = trim_quote_ereg.replace(a,"$1");
            }
            return r;
          }
        });
      }
      
      // execute helper method
      // set the value in context under .loop_name
      Reflect.setField(context,
                       loop_name,
                       Reflect.callMethod(null,
                               Reflect.field(context,helper_name),
                               args
                       )
      ); // Reflect.callMethod(null,resolve(helper_name),args)
    }
    return '::foreach ${loop_name}::';
  }
  
}