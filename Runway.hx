package propeller_crate;

import haxe.unit.TestRunner;
import haxe.unit.TestCase;
import sys.io.File;

class Runway extends TestCase
{
  private static var TEMPLATEPATH =
#if standalone_propeller
    ".";
#else
    "cargo/propeller_crate";
#end
  private static var OLD_PARTIAL_EXTENSION:String;
  private static var OLD_PARTIAL_SEARCH_PATH:Array<String>;

  private var template_basic:String;
  private var context_basic:Dynamic;
  private var rendered_basic:String;

  private var template_full:String;
  private var context_full:Dynamic;
  private var rendered_full:String;

  private var template_helpers:String;
  private var context_helpers:Dynamic;
  private var rendered_helpers:String;

  private var template_indents:String;
  private var rendered_indents:String;  

  public function new()
  {
    super();

    setup();

    // read templates
    template_basic = File.getContent('$TEMPLATEPATH/test_templates/basic.html');
    template_full = File.getContent('$TEMPLATEPATH/test_templates/full_featured.html');
    template_helpers = File.getContent('$TEMPLATEPATH/test_templates/helpers.html');
    template_indents = File.getContent('$TEMPLATEPATH/test_templates/special_indents.html');

    // read expected outputs
    rendered_basic = File.getContent('$TEMPLATEPATH/test_templates/basic.rendered.html');
    rendered_full = File.getContent('$TEMPLATEPATH/test_templates/full_featured.rendered.html');
    rendered_helpers = File.getContent('$TEMPLATEPATH/test_templates/helpers.rendered.html');
    rendered_indents = File.getContent('$TEMPLATEPATH/test_templates/special_indents.rendered.html');

    // create contexts
    context_basic = {
      title : "Propeller Templating System"
    }
    context_full = {
      page : {
        name : "Propeller Templating System",
        a_special_class : "addition"
      },
      user : {
        name : "Propeller User",
        logged_in : true
      },
      helper_test : function():String
      {
        return "Helper Test";
      },
      blogs : Blog.all(),
      getBlogs : function(limit:String, extra:String):Array<Blog>
      {
        return Blog.get(Std.parseInt(limit));
      }
    }
    context_helpers = {
      helper_1 : function(arg_1:String, arg_2:String):String
      {
        return '<span id="${arg_1}">${arg_2}</span>';
      },
      helper_2 : function(arg_3:String, arg_4:String):String
      {
        return '${arg_3} ${arg_4}';
      },
      helper_3 : function():String
      {
        return "Helper 3";
      },
      arg_1 : "Arg1ID",
      arg_3 : "class_from_context",
      unique_page : {
        name : "Propeller"
      }
    }

    tearDown();
  }
  public override function setup():Void
  {
    // Propeller settings
    OLD_PARTIAL_EXTENSION = Propeller.PARTIAL_EXTENSION;
    OLD_PARTIAL_SEARCH_PATH = Propeller.PARTIAL_SEARCH_PATHS;
    Propeller.PARTIAL_EXTENSION = '.html';
    Propeller.PARTIAL_SEARCH_PATHS = ['$TEMPLATEPATH/test_templates'];
  }

  public override function tearDown():Void
  {
    // restore original Propeller settings
    Propeller.PARTIAL_EXTENSION = OLD_PARTIAL_EXTENSION;
    Propeller.PARTIAL_SEARCH_PATHS = OLD_PARTIAL_SEARCH_PATH;
  }
  
  public function test_html_basic():Void
  {
    var propeller = Propeller.render_html(template_basic, context_basic);
    
    assertEquals(rendered_basic, propeller);
  }

  public function test_html_full():Void
  {
    var propeller = Propeller.render_html(template_full, context_full);
    
    // noisy, just use diff and assertTrue
    // assertEquals(rendered_full, propeller);
    if(rendered_full != propeller){
      diff(rendered_full, propeller);
    }

    assertTrue(rendered_full == propeller);
    
  }

  public function test_context_helpers():Void
  {
    var html = Propeller.render_html(template_helpers, context_helpers);
    
    // noisy, just use diff and assertTrue
    // assertEquals(rendered_full, haml);
    if(rendered_helpers != html){
      diff(rendered_helpers, html);
    }

    assertTrue(rendered_helpers == html);
  }

  public function test_special_indents():Void
  {
    var html = Propeller.render_html(template_indents, context_full);
    
    // noisy, just use diff and assertTrue
    // assertEquals(rendered_indents, html);
    if(rendered_indents != html){
      diff(rendered_indents, html);
    }
    
    assertTrue(rendered_indents == html);
  }

  // requires mktemp
  private static inline function diff(a:String, b:String):Void
  {
    // write the tmp files
    var tmpdir = StringTools.trim(
      new sys.io.Process('mktemp', ['-dt',"Propeller_Runway"]).stdout.readAll().toString()
    );
    sys.io.File.saveContent('${tmpdir}/a.tmp', a);
    sys.io.File.saveContent('${tmpdir}/b.tmp', b);
    var diff = new sys.io.Process('diff', ['-C 10','$tmpdir/a.tmp','$tmpdir/b.tmp']).stdout.readAll().toString();
    sys.io.File.saveContent('$tmpdir/propeller.diff', diff);
    trace('\n=== Diff file saved at ===\n$tmpdir/propeller.diff\n==========================');
    //Sys.command("$EDITOR", [tmpdir]);
    Sys.command("/usr/local/bin/subl", [tmpdir]);
  }

  public static function main()
  {
    var r = new TestRunner();
    r.add(new Runway());
    r.run();
  }
}

typedef Img = {
  var path:String;
}

class Blog
{
  public var title:String;
  public var gallery:Array<Img>;
  public var body:String;

  // simulate orm
  public static function all(){
    return [for (i in 0...3){
      new Blog({
        title : 'Post #$i',
        body : 'Body $i',
        gallery : [
          for (k in 0...3){
            {
              path : 'post${i}_$k.png'
            }
          }
        ]
      });
    }];
  }
  public static function get(limit){
    return [for (i in 0...limit){
      new Blog({
        title : 'Post #$i',
        body : 'Body $i',
        gallery : [
          for (k in 0...limit){
            {
              path : 'post${i}_$k.png'
            }
          }
        ]
      });
    }];
  }
  public function new(fields:Dynamic)
  {
    title = fields.title;
    body = fields.body;
    gallery = fields.gallery;
  }
}